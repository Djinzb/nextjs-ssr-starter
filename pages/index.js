import React from 'react';
import Head from 'next/head';
import GlobalStyle from '../theme/globalStyles';
import Layout from '../components/layout';

const Home = () => (
  <>
    <Head>
      <title>Title</title>
    </Head>
    <Layout>
      <GlobalStyle />
      <h1>Hello, world!</h1>
    </Layout>
  </>
);

export default Home;
