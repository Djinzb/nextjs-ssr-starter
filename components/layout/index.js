import React from 'react';
import styled from 'styled-components';

const index = ({ children }) => {
  return <StyledMain>{children}</StyledMain>;
};

const StyledMain = styled.main`
  width: 100vw;
  border: 1px solid red;
`;

export default index;
